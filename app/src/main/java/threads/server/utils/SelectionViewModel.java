package threads.server.utils;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import threads.server.core.threads.SortOrder;

public class SelectionViewModel extends ViewModel {
    @NonNull
    private final MutableLiveData<List<Folder>> folders = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Long> parentThread = new MutableLiveData<>(0L);
    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);
    @NonNull
    private final MutableLiveData<String> query = new MutableLiveData<>("");

    @NonNull
    private final MutableLiveData<SortOrder> sortOrder = new MutableLiveData<>(SortOrder.DATE);

    @NonNull
    private final MutableLiveData<Uri> uri = new MutableLiveData<>(null);

    @NonNull
    public MutableLiveData<Long> getParentThread() {
        return parentThread;
    }

    public void setParentThread(long idx) {
        getParentThread().postValue(idx);
    }

    @NonNull
    public MutableLiveData<String> getQuery() {
        return query;
    }

    public void setQuery(@NonNull String query) {
        getQuery().postValue(query);
    }

    @NonNull
    public MutableLiveData<SortOrder> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(@NonNull SortOrder sortOrder) {
        getSortOrder().postValue(sortOrder);
    }

    @NonNull
    public MutableLiveData<Uri> getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        getUri().postValue(uri);
    }

    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        getShowFab().postValue(show);
    }

    @NonNull
    public MutableLiveData<List<Folder>> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        getFolders().postValue(folders);
    }
}
