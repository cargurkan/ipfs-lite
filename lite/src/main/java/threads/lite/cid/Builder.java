package threads.lite.cid;

public interface Builder {

    Cid sum(byte[] data);

    Builder withCodec(long codec);

}
