package threads.lite.format;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import threads.lite.cid.Builder;
import threads.lite.cid.Cid;

public class RawNode implements Node {

    private final Block block;


    public RawNode(@NonNull Block block) {
        this.block = block;
    }

    public static Node createRawNode(byte[] data, Builder builder) {

        builder = builder.withCodec(Cid.Raw);
        Cid cid = builder.sum(data);

        Block blk = BasicBlock.createBlockWithCid(cid, data);

        return new RawNode(blk);

    }

    @Override
    public Pair<Link, List<String>> resolveLink(@NonNull List<String> path) {
        throw new RuntimeException("not supported here");
    }

    @Override
    public long size() {
        return getData().length;
    }

    @Override
    public List<Link> getLinks() {
        return Collections.emptyList();
    }

    @Override
    public int numLinks() {
        return 0;
    }

    @Override
    public Cid getCid() {
        return block.getCid();
    }

    @Override
    public byte[] getData() {
        return block.getRawData();
    }

    @Override
    public byte[] getRawData() {
        return block.getRawData();
    }

    @Override
    public Pair<Link, List<String>> resolve(@NonNull List<String> path) {
        throw new RuntimeException("not supported here");
    }
}
