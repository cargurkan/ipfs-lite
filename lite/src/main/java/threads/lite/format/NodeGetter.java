package threads.lite.format;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.core.Closeable;

public interface NodeGetter {
    @Nullable
    Node getNode(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException;

    void preload(@NonNull Closeable ctx, @NonNull List<Cid> cids);
}
