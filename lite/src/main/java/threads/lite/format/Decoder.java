package threads.lite.format;

import androidx.annotation.NonNull;

import threads.lite.cid.Cid;

public class Decoder {
    @NonNull
    public static Node Decode(@NonNull Block block) {

        if (block instanceof Node) {
            return (Node) block;
        }

        long type = block.getCid().getType();

        if (type == Cid.DagProtobuf) {
            return decodeProtobufBlock(block);
        } else if (type == Cid.Raw) {
            return decodeRawBlock(block);
        } else if (type == Cid.DagCBOR) {
            throw new RuntimeException("Not supported decoder");
        } else {
            throw new RuntimeException("Not supported decoder");
        }
    }


    public static Node decodeRawBlock(@NonNull Block block) {
        if (block.getCid().getType() != Cid.Raw) {
            throw new RuntimeException("raw nodes cannot be decoded from non-raw blocks");
        }
        return new RawNode(block);
    }

    public static Node decodeProtobufBlock(@NonNull Block block) {
        Cid cid = block.getCid();
        if (cid.getType() != Cid.DagProtobuf) {
            throw new RuntimeException("this function can only decode protobuf nodes");
        }

        return ProtoNode.createProtoNode(cid, block.getRawData());
    }

}
