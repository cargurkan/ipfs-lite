package threads.lite.format;

import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import merkledag.pb.Merkledag;
import threads.lite.cid.Builder;
import threads.lite.cid.Cid;


public class ProtoNode implements Node {

    @NonNull
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    @NonNull
    private final List<Link> links = Collections.synchronizedList(new ArrayList<>());
    private Cid cached;
    private byte[] data;
    private byte[] encoded;
    private Builder builder;

    private ProtoNode(@Nullable byte[] data, @NonNull List<Link> links) {
        this.cached = Cid.Undef();
        this.links.addAll(links);
        this.data = data;
        this.links.sort(Comparator.comparing(Link::getName));
    }

    @NonNull
    public static ProtoNode createProtoNode() {
        return new ProtoNode(null, Collections.emptyList());
    }

    @NonNull
    public static ProtoNode createProtoNode(@NonNull byte[] data, @NonNull List<Link> links) {
        return new ProtoNode(data, links);
    }

    @NonNull
    public static ProtoNode createProtoNode(@NonNull Cid cid, byte[] encoded) {

        try {
            Merkledag.PBNode pbNode = Merkledag.PBNode.parseFrom(encoded);

            List<Link> children = new ArrayList<>();

            List<Merkledag.PBLink> pbLinks = pbNode.getLinksList();
            for (Merkledag.PBLink pbLink : pbLinks) {
                children.add(Link.create(pbLink.getHash().toByteArray(), pbLink.getName(),
                        pbLink.getTsize()));
            }

            ProtoNode protoNode = createProtoNode(pbNode.getData().toByteArray(), children);

            protoNode.encoded = encoded;
            protoNode.cached = cid;

            return protoNode;

        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

    }

    @Override
    public Pair<Link, List<String>> resolveLink(@NonNull List<String> path) {

        if (path.size() == 0) {
            throw new RuntimeException("end of path, no more links to resolve");
        }
        String name = path.get(0);
        Link lnk = getNodeLink(name);
        List<String> left = new ArrayList<>(path);
        left.remove(name);
        return Pair.create(lnk, left);
    }

    @NonNull
    private Link getNodeLink(@NonNull String name) {
        readWriteLock.readLock().lock();
        try {
            for (Link link : links) {
                if (Objects.equals(link.getName(), name)) {
                    return link;
                }
            }
            throw new RuntimeException("" + name + " not found");
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public long size() {
        byte[] b = encodeProtobuf();
        long size = b.length;
        readWriteLock.readLock().lock();
        try {
            for (Link link : links) {
                size += link.getSize();
            }
        } finally {
            readWriteLock.readLock().unlock();
        }
        return size;
    }

    @Override
    public List<Link> getLinks() {
        return Collections.unmodifiableList(links);
    }

    @Override
    public int numLinks() {
        return links.size();
    }

    @Override
    public Cid getCid() {
        if (encoded != null && cached.isDefined()) {
            return cached;
        }
        byte[] data = getRawData();

        if (encoded != null && cached.isDefined()) {
            return cached;
        }
        cached = getCidBuilder().sum(data);
        return cached;
    }

    @Override
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] fileData) {
        encoded = null;
        cached = Cid.Undef();
        data = fileData;
    }

    @Override
    public byte[] getRawData() {
        return encodeProtobuf();
    }


    private byte[] encodeProtobuf() {


        if (encoded == null) {
            cached = Cid.Undef();

            Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();

            try {
                readWriteLock.readLock().lock();

                for (Link link : links) {

                    Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder().setName(link.getName())
                            .setTsize(link.getSize());

                    if (link.getCid().isDefined()) {
                        ByteString hash = ByteString.copyFrom(link.getCid().bytes());
                        lnb.setHash(hash);
                    }

                    pbn.addLinks(lnb.build());
                }
            } finally {
                readWriteLock.readLock().unlock();
            }

            if (this.data.length > 0) {
                pbn.setData(ByteString.copyFrom(this.data));
            }

            encoded = pbn.build().toByteArray();

        }

        if (!cached.isDefined()) {
            cached = getCidBuilder().sum(encoded);
        }

        return encoded;
    }

    @NonNull
    public Builder getCidBuilder() {
        if (builder == null) {
            builder = v0CidPrefix;
        }
        return builder;
    }

    public void setCidBuilder(@Nullable Builder builder) {
        if (builder == null) {
            this.builder = v0CidPrefix;
        } else {
            this.builder = builder.withCodec(Cid.DagProtobuf);
            this.cached = Cid.Undef();
        }
    }

    @NonNull
    public Node copy() {

        ProtoNode protoNode = ProtoNode.createProtoNode(
                Arrays.copyOf(getData(), getData().length), getLinks());

        protoNode.builder = builder;

        return protoNode;


    }

    public void removeNodeLink(@NonNull String name) {
        encoded = null;
        try {
            readWriteLock.writeLock().lock();
            for (Link link : links) {
                if (Objects.equals(link.getName(), name)) {
                    links.remove(link);
                    break;
                }
            }
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void addNodeLink(@NonNull String name, @NonNull Node link) {
        encoded = null;
        try {
            readWriteLock.writeLock().lock();
            links.add(Link.createLink(link, name));
            if (!name.isEmpty()) {
                links.sort(Comparator.comparing(Link::getName));// keep links sorted
            }
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public Pair<Link, List<String>> resolve(@NonNull List<String> path) {
        return resolveLink(path);
    }

}
