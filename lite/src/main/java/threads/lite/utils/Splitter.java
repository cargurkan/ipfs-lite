package threads.lite.utils;

public interface Splitter {

    byte[] nextBytes();

    boolean done();
}
