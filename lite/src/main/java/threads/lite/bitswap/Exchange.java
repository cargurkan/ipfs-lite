package threads.lite.bitswap;


import androidx.annotation.NonNull;

import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.core.Closeable;
import threads.lite.format.Block;

public interface Exchange {
    void reset();

    Block getBlock(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException;

    void preload(@NonNull Closeable closeable, @NonNull List<Cid> cids);
}
